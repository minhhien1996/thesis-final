# coding: utf-8
import tensorflow as tf
import numpy as np
import os

batch_size = 8

from helper import get_dict_embeding

dictionary, reverse_dictionary, word_embedding_matrix, embedding_dim = get_dict_embeding()
vocab_size = len(word_embedding_matrix)
checkpoint = "./checkpoints/seq2seq_0/summarization.ckpt"

input_file = "text"
output_file = "out"
path = 'input'


MAX_LENGTH = 200


def text_to_seq(text):
    '''Prepare the text for the model'''
    return [dictionary.get(word, dictionary["<UNK>"]) for sentence in text.split(' . ') for word in sentence.split()][0:MAX_LENGTH]

# text = text_to_seq(input_sentence)


loaded_graph = tf.Graph()
with tf.Session(graph=loaded_graph) as sess:
    # Load saved model
    loader = tf.train.import_meta_graph(checkpoint + '.meta')
    loader.restore(sess, checkpoint)

    input_data = loaded_graph.get_tensor_by_name('input:0')
    logits = loaded_graph.get_tensor_by_name('predictions:0')
    text_length = loaded_graph.get_tensor_by_name('text_length:0')
    summary_length = loaded_graph.get_tensor_by_name('summary_length:0')
    keep_prob = loaded_graph.get_tensor_by_name('keep_prob:0')

    with open(os.path.join(path, input_file), mode="r", encoding="utf8") as f, open(os.path.join(path, output_file), mode='w', encoding="utf8") as fo:
        i = 0
        batch = []
        for line in f:
            text = text_to_seq(line.strip("\n"))
            batch.append(text)
            if len(batch) == batch_size:
                answer_logits = sess.run(logits, {input_data: batch,
                                                  summary_length: [np.random.randint(20, 40)],
                                                  text_length: [200] * batch_size,
                                                  keep_prob: 1.0})

                batch = []
                for summary in answer_logits:
                    pad = dictionary["<PAD>"]
                    fo.write("{}\n".format(
                        " ".join([reverse_dictionary[i] for i in summary if i != pad])))
            i += 1
