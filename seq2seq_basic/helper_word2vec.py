import tensorflow as tf
import numpy as np
import json
import os
import pickle
import datetime

DATA_PATH = 'word2vec_data'
embedding_filename = 'zingnews_embedding_10000_128'

dictionary = None
reverse_dictionary = None

def load_embedding_from_file():
    with open(os.path.join(DATA_PATH, embedding_filename), mode='r', encoding='utf-8') as embedding_file:
        embedding = dict()
        for line in embedding_file:
            splitted = line.split(' ')
            word = splitted[0]
            vector = [float(value) for value in splitted[1:]]
            embedding[word] = vector
        return embedding

def get_dictionary():
  embedding = load_embedding_from_file()
  count = 0
  word_to_int = {}
  int_to_word = []
  embedding_matrix = []
  for (word, vector) in embedding.items():
    word_to_int[word] = count
    int_to_word.append(word)
    embedding_matrix.append(vector)
    count += 1
  return word_to_int, int_to_word, embedding_matrix
  


def get_helpers(codes=['<S>', '</S>', '<UNK>', '<PAD>']):
  embedding_dim = 128
  word_to_int, int_to_word, embedding_matrix = get_dictionary()
  
  random = np.random.randint(0, len(int_to_word))
  before_adding_codes = embedding_matrix[random]

  int_to_word = codes + int_to_word
  for (word, _) in word_to_int.items():
    word_to_int[word] += len(codes)
  for i in range(len(codes)):
    code = codes[i]
    word_to_int[code] = i
    assert int_to_word[word_to_int[code]] == code
    embedding_matrix = [np.array(np.random.uniform(-1.0, 1.0, embedding_dim),
                                 dtype="float32").tolist()] + embedding_matrix
  embedding_matrix = np.array(embedding_matrix)

  # test
  after_addding_codes = embedding_matrix[random + len(codes)]

  assert(np.equal(before_adding_codes, after_addding_codes)[0])

  return word_to_int, int_to_word, embedding_matrix, embedding_dim