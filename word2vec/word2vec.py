import numpy as np
import tensorflow as tf
import math
import os
import argparse
import pickle
import json
from tensorflow.contrib.tensorboard.plugins import projector
from input_helper import generate_batch

parser = argparse.ArgumentParser()

VOCAB_SIZE = -1
BATCH_SIZE = 128

# Dimension of the embedding vector.
EMBEDDING_SIZE = 128

# How many words to consider left and right.
SKIP_WINDOW = 2

# How many times to reuse an input to generate a label.
NUM_SKIPS = 4

NUM_STEPS = 200001

DATASOURCE = [
  # 'tuoitre',
  'zingnews'
]

MODEL_TYPE = "_".join(DATASOURCE)

DATA_PATH = os.path.join('data', MODEL_TYPE)
PARAMS_FILENAME = 'params.json'

data = None
dictionary = None
reverse_dictionary = None

with open(os.path.join(DATA_PATH, PARAMS_FILENAME), 'r') as f:
  params = json.load(f)
  VOCAB_SIZE = params["vocabulary_size"]
  with open(os.path.join(DATA_PATH, params["data_filename"]), 'rb') as f:
    data = pickle.load(f)  

  with open(os.path.join(DATA_PATH, params["dict_filename"]), 'rb') as f:
    dictionary = pickle.load(f)  

  with open(os.path.join(DATA_PATH, params["rev_dict_filename"]), 'rb') as f:
    reverse_dictionary = pickle.load(f)
 
MODEL_NAME = '{}_softmax_{}_{}_{}_{}'.format(MODEL_TYPE, str(EMBEDDING_SIZE), str(VOCAB_SIZE), str(NUM_SKIPS), str(SKIP_WINDOW))
LOG_DIR = os.path.join('checkpoints', MODEL_NAME)

# Number of negative examples to sample.
NUM_SAMPLED = 64

LEARNING_RATE = 1.0

# store meta file for later visualization
META_FILENAME = 'metadata.tsv'

if not os.path.exists(LOG_DIR):
  os.makedirs(LOG_DIR)
#  data, count, dictionary, reverse_dictionary = generate_data(filename='text.txt', vocabulary_size=VOCAB_SIZE)

with open(os.path.join(LOG_DIR, META_FILENAME), mode='w', encoding="utf8") as f:
  for word, _ in dictionary.items():
    f.write(word + '\n')

def my_generator():
  our_generator = generate_batch(data=data, batch_size=BATCH_SIZE, num_skips=NUM_SKIPS, skip_window=SKIP_WINDOW)
  # count = 0
  while True:
    batch = next(our_generator)
    # features, labels = batch
    # for i in range(BATCH_SIZE):
    #   feat_word = features[i]
    #   label_word = labels[i][0]
    #   print("Batch num {} context {} - {} \n".format(count, reverse_dictionary[feat_word], reverse_dictionary[label_word]))
    # count += 1
    yield batch

def train_input_fn():
  """Train input function for Word2Vec Skip-gram model"""
  return tf.data.Dataset.from_generator(
      generator=my_generator,
      output_types=(tf.int32, tf.int32),
      output_shapes=([BATCH_SIZE], [BATCH_SIZE, 1])
  )

def my_model(features, labels, mode, params):
  """Word2Vec Skip-gram model"""

  vocab_size = params["vocab_size"]
  embedding_size = params["embedding_size"]
  num_sampled = params["num_sampled"]
  learning_rate= params["learning_rate"]
  meta_filename = params["meta_filename"]
  log_dir = params["log_dir"]


  embeddings = tf.Variable(
      tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0), name="unnormalized_embedding")
  embed = tf.nn.embedding_lookup(embeddings, features, name="embed")

  norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True), name="norm")
  normalized_embeddings = tf.Variable(
      tf.div(x=embeddings, y=norm), name="word_embedding")
  # Write config for tensorboard projection
  config = projector.ProjectorConfig()

  word_embedding_config = config.embeddings.add()
  word_embedding_config.tensor_name = normalized_embeddings.name
  word_embedding_config.metadata_path = meta_filename

  unorm_embedding_config = config.embeddings.add()
  unorm_embedding_config.tensor_name = embeddings.name
  unorm_embedding_config.metadata_path = meta_filename

  # Use the same LOG_DIR where you stored your checkpoint.
  summary_writer = tf.summary.FileWriter(log_dir)

  # The next line writes a projector_config.pbtxt in the LOG_DIR. TensorBoard will
  # read this file during startup.
  projector.visualize_embeddings(summary_writer, config)
  
  nce_weights = tf.Variable(
      tf.truncated_normal([vocab_size, embedding_size],
                          stddev=1.0 / math.sqrt(embedding_size)))
  nce_biases = tf.Variable(tf.zeros([vocab_size]))

  # tf.nce_loss automatically draws a new sample of the negative labels each
  # time we evaluate the loss.
  # Explanation of the meaning of NCE loss:
  #   http://mccormickml.com/2016/04/19/word2vec-tutorial-the-skip-gram-model/
  loss = tf.reduce_mean(
      tf.nn.nce_loss(weights=nce_weights,
                     biases=nce_biases,
                     labels=labels,
                     inputs=embed,
                     num_sampled=num_sampled,
                     num_classes=vocab_size))
  # Create the optimizer
  # Construct the SGD optimizer using a learning rate of 1.0.
  optimizer = tf.train.GradientDescentOptimizer(
      learning_rate=learning_rate)
  train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
    
  return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)

def predict_input_fn(word):
  """An input function for training"""
  # Convert the inputs to a Dataset.
  features = [dictionary.get(word, 0)] * BATCH_SIZE
  fake_labels = [[0]] * BATCH_SIZE
  return (features, fake_labels)


def main(argv):
  args = parser.parse_args(argv[1:])

  estimator = tf.estimator.Estimator(
      model_fn=my_model,
      params={
        "vocab_size": VOCAB_SIZE,
        "embedding_size": EMBEDDING_SIZE,
        "num_sampled": NUM_SAMPLED,
        "learning_rate": LEARNING_RATE,
        "meta_filename": META_FILENAME,
        "log_dir": LOG_DIR
      },
      model_dir=LOG_DIR
  )

  # Train the Model.
  estimator.train(input_fn=train_input_fn, steps=NUM_STEPS)


if __name__ == '__main__':
  tf.logging.set_verbosity(tf.logging.INFO)
  tf.app.run(main)