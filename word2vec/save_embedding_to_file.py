# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np
import json
import os
import pickle
import datetime
from input_helper import preprocess

DATA_PATH = 'data/medium'
MODEL_NAME = 'medium_softmax_128_5000_4_2'
LOG_DIR = os.path.join('checkpoints', MODEL_NAME)
PARAMS_FILENAME = 'params.json'

data = None
dictionary = None
reverse_dictionary = None
VOCAB_SIZE = -1
EMBEDDING_SIZE = 128


with open(os.path.join(DATA_PATH, PARAMS_FILENAME), 'r') as f:
    params = json.load(f)
    VOCAB_SIZE = params["vocabulary_size"]
    with open(os.path.join(DATA_PATH, params["data_filename"]), 'rb') as f:
        data = pickle.load(f)  

    with open(os.path.join(DATA_PATH, params["dict_filename"]), 'rb') as f:
        dictionary = pickle.load(f)  

    with open(os.path.join(DATA_PATH, params["rev_dict_filename"]), 'rb') as f:
        reverse_dictionary = pickle.load(f)

embedding_filename  = 'medium_embedding_{}_{}'.format(VOCAB_SIZE, EMBEDDING_SIZE)

def save_embedding_to_file():
    graph = tf.Graph()

    with graph.as_default():
        normalized_embedding = tf.Variable(tf.random_uniform([VOCAB_SIZE, EMBEDDING_SIZE], -1.0, 1.0), name="unnormalized_embedding")
        saver = tf.train.Saver()

    with tf.Session(graph=graph) as session:
        saver.restore(session, os.path.join(LOG_DIR, 'model.ckpt-200001'))
        normalized_embedding_tensor = tf.get_default_graph().get_tensor_by_name("unnormalized_embedding:0")
        normalized_embedding_value = normalized_embedding_tensor.eval()
        OUTPUT = open(os.path.join(DATA_PATH, embedding_filename), mode='w')
        for i in range(1, VOCAB_SIZE): # 0 is UNK
            OUTPUT.write(reverse_dictionary[i])
            for j in range(EMBEDDING_SIZE):
                OUTPUT.write(' ')
                OUTPUT.write("%.2f" % normalized_embedding_value[i][j])
            OUTPUT.write('\n')
    OUTPUT.close()


def load_embedding_from_file():
    with open(os.path.join(DATA_PATH, embedding_filename), mode='r') as embedding_file:
        embedding = dict()
        for line in embedding_file:
            splitted = line.split(' ')
            word = splitted[0]
            vector = [float(value) for value in splitted[1:]]
            embedding[word] = vector
        return embedding

def test():
    if not os.path.exists(os.path.join(DATA_PATH, embedding_filename)):
        save_embedding_to_file()
    embedding = load_embedding_from_file()

    embedding_matrix = np.zeros((VOCAB_SIZE, EMBEDDING_SIZE), dtype=np.float32)
    for word, idx in dictionary.items():
        embedding_matrix[idx] = embedding.get(word, np.array(np.random.uniform(-1.0, 1.0, EMBEDDING_SIZE)))
    # start = datetime.datetime.now()
    # words = ["microsoft", "javascript", "nodejs", "reactjs", "python", "java", "android", "facebook", "love", "es6", "apache_cassandra", "apache_kafka", "django", "react_native"]
    # batch =[embedding.get(word, np.array(np.random.uniform(-1.0, 1.0, EMBEDDING_SIZE))) for word in words]
    # similarity = np.matmul(np.array(batch), np.transpose(embedding_matrix))
    # nearests = (-similarity).argsort(axis=-1)
    # for i in range(len(words)):
    #     print(words[i], [reverse_dictionary[k] for k in nearests[i][1:20]])
    # finish = datetime.datetime.now()
    # print("PROCESS TIME {}".format(finish-start))

    # negative  = ["java"]
    # positive = ["android", "ios"]
    
    # neg_vec = np.sum([embedding.get(word, np.array(np.random.uniform(-1.0, 1.0, EMBEDDING_SIZE))) for word in negative], axis=0)
    # pos_vec = np.sum([embedding.get(word, np.array(np.random.uniform(-1.0, 1.0, EMBEDDING_SIZE))) for word in positive], axis=0)
    # vector = np.subtract(pos_vec, neg_vec)
    # batch =[vector]
    # similarity = np.matmul(np.array(batch), np.transpose(embedding_matrix))
    # nearests = (-similarity).argsort(axis=-1)
    # print([reverse_dictionary[k] for k in nearests[0][0:20]])

    with open('text.txt', mode='r', encoding='utf-8') as f, open('data/phrases_processed.txt', mode='r', encoding='utf-8') as fp:
        phrases = []
        for line in fp:
            phrase = line.rstrip("\n")
            phrases.append(phrase)
        phrases_int = [dictionary[word] for word in phrases if dictionary.get(word, None)]
        batch = []
        doc_list = []
        for line in f:
            text = line.rstrip('\n')
            sentences = text.split(' . ')
            words = [word for sentence in sentences for word in sentence.split()]
            def get_vector_for_word(word_id, reverse_dictionary, phrases_int, embedding):
                word = reverse_dictionary[word_id]
                if word_id in phrases_int:
                    return np.sum([embedding[word], embedding[word]], axis=0)
                else:
                    return embedding[word]
            # remove stop word
            # convert to id
            word_ids = ([dictionary[word] for  word in words if dictionary.get(word, None) and word not in stopwords])
            
            # sum up 
            vector = np.sum([get_vector_for_word(word_id, reverse_dictionary, phrases_int, embedding) for word_id in word_ids], axis=0)
            batch.append(vector)

            # keep id for later filter
            doc_list.append(word_ids)
        similarity = np.matmul(np.array(batch), np.transpose(embedding_matrix))
        nearests = (-similarity).argsort(axis=-1)
        for i in range(len(nearests)):
            nearest = nearests[i]
            doc = doc_list[i]
            candidates = [k for k in nearest][0:150]
            in_doc_keywords = [k for k in candidates if k in doc]
            not_in_doc_keywords = [k for k in candidates if not k in doc]
            not_in_doc_but_in_list = [k for k in not_in_doc_keywords if k in phrases_int]

            # find words that relate to all keywords in doc
            minibatch = np.sum([embedding_matrix[k] for k in in_doc_keywords], axis=0)
            similarity = np.matmul(np.array([minibatch]), np.transpose(embedding_matrix))
            nearests_ = (-similarity).argsort(axis=-1)[0][0:10]
            # intersection with not_in_doc_but_in_list
            not_in_doc_candidate = list(set(nearests_).intersection(set(not_in_doc_but_in_list)))

            final = [k for k in in_doc_keywords if k in phrases_int] + not_in_doc_candidate[0:10] 
            print([reverse_dictionary[k] for k in final][0:20])

test()