# coding=utf8
import os
import re
import collections
import unicodedata
import sys
import numpy as np
import random
import pymongo
import json
import pickle
import logging
import string

DATA_PATH = 'data'
PHRASES_LIST_FILE = 'phrases_hien.txt'
PHRASES_LIST_FILE_PRE = 'phrases_hien_processed.txt'
PLURAL_FILE = 'plural.txt'

MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_DATABASE = 'medium'
MONGODB_STREAM_COLLECTION = 'streams'
MONGODB_POST_COLLECTION = 'posts'

MONGODB_DEVDOCS_DATABASE = 'devdocs'
MONGODB_DOCS_COLLECTION = 'docs'

DATASOURCE = [
  # 'tuoitre',
  'zingnews'
]
MODEL_TYPE = "_".join(DATASOURCE)

DATASOURCE_CONFIG = {
  "zingnews": {
    "filename": "tokens"
  },
  "tuoitre": {
    "filename": "tokens_TTO"
  }
}


META_FILENAME = 'metadata.tsv'

DICT_FILENAME = '{}_dict'.format(MODEL_TYPE)
REV_DICT_FILENAME = '{}_rev_dict'.format(MODEL_TYPE)
DATA_FILENAME = '{}_data'.format(MODEL_TYPE)
PARAMS_FILENAME = 'params.json'

VOCAB_SIZE = 10000
LOG_DIR = os.path.join(DATA_PATH, MODEL_TYPE)

def generate_data(vocabulary_size):
  count = [['<UNK>', -1]]
  vocabulary = []

  for source in DATASOURCE:
    filename = DATASOURCE_CONFIG[source]['filename']
    with open(os.path.join(DATA_PATH, filename), mode='r', encoding='utf-8') as f:
      for line in f:
        content = line.strip("\n").split(' . ')
        for sentence in content:
          vocabulary += sentence.split(" ")

  print('Data size', len(vocabulary))
  count.extend(collections.Counter(vocabulary).most_common(vocabulary_size - 1))
  dictionary = dict()
  for word, _ in count:
    dictionary[word] = len(dictionary)
  data = list()
  unk_count = 0
  for word in vocabulary:
    index = dictionary.get(word, 0)
    if index == 0:  # dictionary['UNK']
      unk_count += 1
    data.append(index)
  count[0][1] = unk_count
  print('unk_count', unk_count)
  reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))

  return data, count, dictionary, reverse_dictionary

def generate_batch(data, batch_size, num_skips, skip_window):
  assert batch_size % num_skips == 0
  assert num_skips <= 2 * skip_window
  # print("BEFORE WHILE LOOP len data {}\n".format(len(data)))
  count = 0
  data_index = 0
  while True:
    # print("STEP {}".format(count))
    count += 1

    batch = np.ndarray(shape=(batch_size), dtype=np.int32)
    labels = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
    span = 2 * skip_window + 1  # [ skip_window target skip_window ]
    buffer = collections.deque(maxlen=span)

    # reset data_index if reach the end of data
    if data_index + span > len(data):
      data_index = 0
    
    # print("DATA_INDEX for BUFFER {}\n".format(data_index))
    buffer.extend(data[data_index:data_index + span])
    data_index += span
    # print("DATA_INDEX before FOR LOOP {}\n".format(data_index))

    for i in range(batch_size // num_skips):
      # [ skip_window target skip_window ]
      context_words = [w for w in range(span) if w != skip_window]
      words_to_use = random.sample(context_words, num_skips)
      for j, context_word in enumerate(words_to_use):
        batch[i * num_skips + j] = buffer[skip_window]
        labels[i * num_skips + j, 0] = buffer[context_word]

      if data_index == len(data):
        # reach the end of the list, return to the start
        # buffer[:] = data[:span]
        for word in data[:span]:
          buffer.append(word)
        data_index = span
      else:
        # continue moving
        buffer.append(data[data_index])
        data_index += 1

    # Backtrack a little bit to avoid skipping words in the end of a batch
    data_index = (data_index + len(data) - span) % len(data)
    # print("DATA INDEX {}\n".format(data_index))
    yield batch, labels

def generate_data_to_file():
  if not os.path.exists(LOG_DIR):
    os.makedirs(LOG_DIR)

  with open(os.path.join(LOG_DIR, PARAMS_FILENAME), 'w') as f:
    json.dump({
      'vocabulary_size': VOCAB_SIZE,
      'dict_filename': DICT_FILENAME,
      'rev_dict_filename': REV_DICT_FILENAME,
      'data_filename': DATA_FILENAME
    }, f)

  logging.warning('Generating data...')

  data, _, dictionary, reverse_dictionary = generate_data(vocabulary_size=VOCAB_SIZE)

  logging.warning('Writing data and vocabulary to files...')

  with open(os.path.join(LOG_DIR, META_FILENAME), mode='w', encoding="utf8") as f:
    for word, _ in dictionary.items():
      f.write(word + '\n')

  with open(os.path.join(LOG_DIR, DATA_FILENAME), 'wb') as f:
    pickle.dump(data, f)  

  with open(os.path.join(LOG_DIR, DICT_FILENAME), 'wb') as f:
    pickle.dump(dictionary, f)  

  with open(os.path.join(LOG_DIR, REV_DICT_FILENAME), 'wb') as f:
    pickle.dump(reverse_dictionary, f)
