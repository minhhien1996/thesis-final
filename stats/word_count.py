import os
import numpy as np
from collections import Counter
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

INPUT_PATH = 'input'
TOKENS_FILENAME = 'tokens'
CONTENTS_FILENAME = 'contents'
SUMMARIES_FILENAME = 'summaries'
TITLES_FILENAME = 'titles'

word_counter = Counter()
num_word_sentences_counter = Counter()
num_sentence_contents_counter = Counter()
num_sentence_summaries_counter = Counter()
num_word_titles_counter = Counter()
num_word_summaries_counter = Counter()
num_word_contents_counter = Counter()

with open(os.path.join(INPUT_PATH, TOKENS_FILENAME), mode='r', encoding='utf-8') as f:
    for line in f:
        for word in line.strip('\n').replace(' .', '').split():
            word_counter[word] += 1

log_num = 100

with open(os.path.join(INPUT_PATH, CONTENTS_FILENAME), mode='r', encoding='utf-8') as f:
    for line in f:
        content_sentences = line.strip('\n').split('.')
        num_sentence = len(content_sentences)
        num_sentence_contents_counter[num_sentence] += 1
        num_words = 0
        for sentence in content_sentences:
            num_word_sentences = len(sentence.strip().split(' '))
            num_word_sentences_counter[num_word_sentences] += 1
            num_words += num_word_sentences
        num_word_contents_counter[num_words] += 1

with open(os.path.join(INPUT_PATH, SUMMARIES_FILENAME), mode='r', encoding='utf-8') as f:
    for line in f:
        summary_sentences = line.strip('\n').split('.')
        num_sentence = len(summary_sentences)
        num_sentence_summaries_counter[num_sentence] += 1
        num_words = 0
        for sentence in summary_sentences:
            num_word_sentences = len(sentence.strip().split(' '))
            num_word_sentences_counter[num_word_sentences] += 1
            num_words += num_word_sentences
        num_word_summaries_counter[num_words] += 1

with open(os.path.join(INPUT_PATH, TITLES_FILENAME), mode='r', encoding='utf-8') as f:
    for line in f:
        title_words = line.strip('\n').replace(' . ', ' ').split()
        num_words = len(title_words)
        num_word_titles_counter[num_words] += 1

###############################################################
#
# WRONG CASE
#
# domain, such as `example . com .vn`
# address, such as `p 7 . q 3 . tp . hcm`
# name, such as `j. k. rowling harry potter .`
# end of abbriviation, such as `ps. i love you .`
# author's name and source, such as `minhhien theo vietnamnet`
# wrong split 2 sentences because of all the above cases
# if (num_word_sentences > 3):
#
###############################################################


def histogram_counter(counters):
    plt.figure(1)
    for i in range(len(counters)):
        counter = counters[i]
        print(counter.most_common(10))
        # * for unpack container
        labels, values = zip(*word_counter.items())
        num_bins = 10
        plt.ylabel('No of times')
        plt.subplot((len(counter) / 3) + 1, 3, i + 1)
        n, bins, patches = plt.hist(
            values, num_bins, facecolor='blue', alpha=0.5)
    plt.show()

# histogram_counter([word_counter, num_word_sentences_counter, num_sentence_contents_counter, num_sentence_summaries_counter])


def write_counter(counter, path, filename):
    with open(os.path.join(path, filename), mode='w', encoding='utf-8') as f:
        for ele, count in counter.most_common():
            f.write("{} {}\n".format(ele, count))


OUTPUT = 'output'

if not os.path.exists(OUTPUT):
    os.makedirs(OUTPUT)


write_counter(word_counter, path=OUTPUT, filename="word_counter")
write_counter(num_word_sentences_counter, path=OUTPUT,
              filename="num_word_sentences_counter")
write_counter(num_sentence_contents_counter, path=OUTPUT,
              filename="num_sentence_contents_counter")
write_counter(num_sentence_summaries_counter, path=OUTPUT,
              filename="num_sentence_summaries_counter")
write_counter(num_word_titles_counter, path=OUTPUT,
              filename="num_word_titles_counter")
write_counter(num_word_summaries_counter, path=OUTPUT,
              filename="num_word_summaries_counter")
write_counter(num_word_contents_counter, path=OUTPUT,
              filename="num_word_contents_counter")
