from collections import Counter
import os
PATH = 'output'
import numpy as np


def read_counter(file_name):
    counter = Counter()
    with open(os.path.join(PATH, file_name), 'r') as f:
        for line in f:
            value, count = line.rstrip('\n').split(' ')
            value = int(value)
            count = int(count)
            counter[value] = count
    return counter


def percent_tile(file_name, pt=90):
    counter = read_counter(file_name)
    data = list(counter.elements())
    print(np.percentile(data, pt))


percent_tile('num_sentence_contents_counter')
