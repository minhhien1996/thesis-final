import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import os
import numpy as np

INPUT = 'output'
OUTPUT = 'chart'


def plot_scatter(in_path, in_filename, out_path, out_filename):
    with open(os.path.join(in_path, in_filename), mode='r') as f:
        labels = []
        values = []
        for line in f:
            label, value = line.strip('\n').split()
            labels.append(int(label))
            values.append(int(value))

        colors = (0, 0, 0)
        fig = plt.figure()
        plt.scatter(labels, values, c=colors, alpha=0.5)
        plt.xlabel(in_filename)
        plt.ylabel('frequency')
        # plt.show()
        fig.savefig(os.path.join(out_path, out_filename), dpi='figure')


def plot_bar(in_path, in_filename, out_path, out_filename):
    with open(os.path.join(in_path, in_filename), mode='r') as f:
        labels = []
        values = []
        for line in f:
            label, value = line.strip('\n').split()
            labels.append(int(label))
            values.append(int(value))

        y_pos = np.arange(len(values))

        fig = plt.figure()
        plt.bar(y_pos, values, align='center', alpha=0.5)
        plt.xticks(y_pos, labels)
        plt.xlabel(in_filename)
        plt.ylabel('frequency')
        # plt.show()
        fig.savefig(os.path.join(out_path, out_filename), dpi='figure')


plot_scatter(INPUT, 'num_word_contents_counter',
             OUTPUT, 'num_word_contents_counter')
plot_scatter(INPUT, 'num_sentence_contents_counter',
             OUTPUT, 'num_sentence_contents_counter')
plot_scatter(INPUT, 'num_word_sentences_counter',
             OUTPUT, 'num_word_sentences_counter')
plot_scatter(INPUT, 'num_word_summaries_counter',
             OUTPUT, 'num_word_summaries_counter')

plot_bar(INPUT, 'num_sentence_summaries_counter',
         OUTPUT, 'num_sentence_summaries_counter')
plot_bar(INPUT, 'num_word_titles_counter', OUTPUT, 'num_word_titles_counter')
