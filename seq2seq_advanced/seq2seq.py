import json
import logging
import os

import numpy as np
import tensorflow as tf
import timeline
from tensorflow.contrib import layers
from helper_gensim import get_helpers

GO_TOKEN = 0
END_TOKEN = 1
UNK_TOKEN = 2
PAD = 3

DATA_DIR = 'data'
CHECKPOINTS_DIR = 'checkpoints'
MODEL_NAME = 'seq2seq_title_tto_1'

codes = ['<S>', '</S>', '<UNK>', '<PAD>']
word_to_int, int_to_word, embedding_matrix, embedding_dim = get_helpers(
    codes=codes)

input_min_length = 50
output_min_length = 5
input_max_unk = 50

PARAMS = {
    # 'vocab_size': len(vocab),
    'batch_size': 2,
    'input_max_length': 150,
    'output_max_length': 20,
    'embed_dim': 128,
    'beam_width': 3,
    'num_layers': 2,
    'keep_prob_encoder': 0.6,
    'keep_prob_decoder': 0.6,
    'scheduled_sampling': True,
    'num_units': 512
}


def seq2seq(mode, features, labels, params):
    vocab_size = params['vocab_size']
    embed_dim = params['embed_dim']
    num_units = params['num_units']
    num_layers = params['num_layers']
    keep_prob_encoder = params['keep_prob_encoder'] if mode == tf.estimator.ModeKeys.TRAIN else 1
    keep_prob_decoder = params['keep_prob_decoder'] if mode == tf.estimator.ModeKeys.TRAIN else 1
    input_max_length = params['input_max_length']
    output_max_length = params['output_max_length']

    inp = features['input']
    output = features['output']
    batch_size = params['batch_size']
    beam_width = params['beam_width']
    start_tokens = tf.zeros([batch_size], dtype=tf.int32)
    train_output = tf.concat([tf.expand_dims(start_tokens, 1), output], 1)
    input_lengths = tf.reduce_sum(tf.to_int32(tf.not_equal(inp, 1)), 1)
    output_lengths = tf.reduce_sum(
        tf.to_int32(tf.not_equal(train_output, 1)), 1)

    scheduled_sampling = params['scheduled_sampling']

    with tf.variable_scope('embed'):
        embeddings = tf.get_variable(name="embeddings", shape=embedding_matrix.shape,
                                     initializer=tf.constant_initializer(embedding_matrix), trainable=False)

    input_embed = tf.nn.embedding_lookup(embeddings, inp)
    output_embed = tf.nn.embedding_lookup(embeddings, train_output)

    # Encoder
    def encode(input_embed, num_layers):
        rnn_input = input_embed
        encoder_outputs = None
        encoder_states = None
        cell_fw = []
        cell_bw = []
        for layer in range(num_layers):
            with tf.variable_scope('encoder_{}'.format(layer), reuse=False):
                # with tf.variable_scope("sentence_encoder", reuse=False):
                cell_encoder_fw = tf.contrib.rnn.LSTMCell(num_units=num_units)
                cell_encoder_fw = tf.contrib.rnn.DropoutWrapper(
                    cell_encoder_fw, input_keep_prob=keep_prob_encoder)

                cell_encoder_bw = tf.contrib.rnn.LSTMCell(num_units=num_units)
                cell_encoder_bw = tf.contrib.rnn.DropoutWrapper(
                    cell_encoder_bw, input_keep_prob=keep_prob_encoder)

                cell_fw.append(cell_encoder_fw)
                cell_bw.append(cell_encoder_bw)

        cell_fw = tf.contrib.rnn.MultiRNNCell(cell_fw)
        cell_bw = tf.contrib.rnn.MultiRNNCell(cell_bw)
        encoder_outputs, encoder_states = tf.nn.bidirectional_dynamic_rnn(
            cell_fw=cell_fw,
            cell_bw=cell_bw,
            inputs=rnn_input,
            swap_memory=True,
            dtype=tf.float32)
        encoder_outputs = tf.concat(encoder_outputs, 2)

        return encoder_outputs, encoder_states[-1]

    encoder_outputs, encoder_states = encode(input_embed, num_layers)
    encoder_states = encoder_states[0]

    def beam_search_decoder(
            scope, beam_width, start_tokens, end_token,
            encoder_final_outputs, encoder_final_state, output_lengths,
            reuse=None):
        with tf.variable_scope(scope, reuse=reuse):
            tiled_encoder_outputs = tf.contrib.seq2seq.tile_batch(
                encoder_final_outputs, multiplier=beam_width)
            tiled_encoder_final_state = tf.contrib.seq2seq.tile_batch(
                encoder_final_state, multiplier=beam_width)
            tiled_output_lengths = tf.contrib.seq2seq.tile_batch(
                output_lengths, multiplier=beam_width)
            attention_mechanism_beam = tf.contrib.seq2seq.LuongAttention(
                num_units=num_units,
                memory=tiled_encoder_outputs,
                memory_sequence_length=tiled_output_lengths,
                scale=True
            )

            cell = tf.contrib.rnn.LSTMCell(
                num_units=num_units)
            cell = tf.contrib.rnn.DropoutWrapper(
                cell, input_keep_prob=keep_prob_decoder)

            # attn_cell
            attn_cell = tf.contrib.seq2seq.AttentionWrapper(
                cell, attention_mechanism_beam, attention_layer_size=num_units)
            # out_cell = tf.contrib.rnn.OutputProjectionWrapper(
            #     attn_cell, vocab_size, reuse=reuse
            # )
            out_cell = attn_cell

        decoder_initial_state_beam = attn_cell.zero_state(
            dtype=tf.float32, batch_size=batch_size * beam_width)
        decoder_initial_state_beam = decoder_initial_state_beam.clone(
            cell_state=tiled_encoder_final_state)

        projection_layer = tf.layers.Dense(
            vocab_size, use_bias=False)

        decoder = tf.contrib.seq2seq.BeamSearchDecoder(
            cell=out_cell,
            embedding=embeddings,
            start_tokens=start_tokens,
            end_token=END_TOKEN,
            initial_state=decoder_initial_state_beam,
            beam_width=beam_width,
            output_layer=projection_layer,
            # length_penalty_weight=0.0
        )

        outputs = tf.contrib.seq2seq.dynamic_decode(
            decoder=decoder, output_time_major=False,
            impute_finished=False, maximum_iterations=output_max_length
        )
        return outputs[0]

    def training_decoder(helper, scope, encoder_final_outputs, encoder_final_states, reuse=None, ):
        with tf.variable_scope(scope, reuse=reuse):
            attention_mechanism = tf.contrib.seq2seq.LuongAttention(
                num_units=num_units,
                memory=encoder_final_outputs,
                memory_sequence_length=output_lengths,
                scale=True
            )

            # cell = tf.contrib.rnn.GRUCell(num_units=num_units)
            cell = tf.contrib.rnn.MultiRNNCell(
                [tf.contrib.rnn.LSTMCell(num_units=num_units) for _ in range(num_layers)])
            cell = tf.contrib.rnn.DropoutWrapper(
                cell, input_keep_prob=keep_prob_decoder)

            # attn_cell
            attn_cell = tf.contrib.seq2seq.AttentionWrapper(
                cell, attention_mechanism, attention_layer_size=num_units)
            out_cell = tf.contrib.rnn.OutputProjectionWrapper(
                attn_cell, vocab_size, reuse=reuse
            )

            decoder_initial_state = out_cell.zero_state(
                dtype=tf.float32, batch_size=batch_size)
            decoder = tf.contrib.seq2seq.BasicDecoder(
                cell=out_cell, helper=helper,
                initial_state=decoder_initial_state)
        # initial_state=encoder_final_state)

            outputs = tf.contrib.seq2seq.dynamic_decode(
                decoder=decoder, output_time_major=False,
                swap_memory=True,
                impute_finished=True, maximum_iterations=output_max_length
            )
            return outputs[0]

        outputs = tf.contrib.seq2seq.dynamic_decode(
            decoder=decoder, output_time_major=False,
            swap_memory=True,
            impute_finished=True, maximum_iterations=output_max_length
        )
        return outputs[0]

    train_helper = tf.contrib.seq2seq.ScheduledEmbeddingTrainingHelper(
        output_embed, output_lengths, embeddings, 0.3) if scheduled_sampling else tf.contrib.seq2seq.TrainingHelper(
        output_embed, output_lengths)
    # train_helper = tf.contrib.seq2seq.ScheduledEmbeddingTrainingHelper(
    #     output_embed, output_lengths, embeddings, 0.3
    # )

    train_outputs = training_decoder(
        helper=train_helper, scope='decode',
        encoder_final_outputs=encoder_outputs,
        encoder_final_states=encoder_states)
    tf.identity(train_outputs.sample_id[0], name='train_pred_0')

    weights = tf.to_float(tf.not_equal(train_output[:, :-1], 1))
    loss = tf.contrib.seq2seq.sequence_loss(
        train_outputs.rnn_output, output, weights=weights)
    train_op = layers.optimize_loss(
        loss, tf.train.get_global_step(),
        optimizer=params.get('optimizer', 'Adam'),
        learning_rate=params.get('learning_rate', 0.001),
        summaries=['loss', 'learning_rate'])

    if (beam_width):
        ############
        # BEAM SEARCH
        pred_outputs = beam_search_decoder(
            scope='decode',
            beam_width=beam_width,
            start_tokens=start_tokens,
            end_token=END_TOKEN,
            encoder_final_state=encoder_states,
            encoder_final_outputs=encoder_outputs,
            output_lengths=output_lengths,
            reuse=True
        )

        # predicted_ids[0] best to worst, so get the best first
        # [batch_size, T, beam_width]
        tf.identity(pred_outputs.predicted_ids[0, :, 0], name='predictions_0')
        ############
        return tf.estimator.EstimatorSpec(
            mode=mode,
            #######
            # BEAM SEARCH
            predictions=pred_outputs.predicted_ids[:, :0],
            loss=loss,
            train_op=train_op
        )

    pred_helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
        embeddings, start_tokens=start_tokens, end_token=END_TOKEN)

    pred_outputs = training_decoder(
        helper=pred_helper,
        encoder_final_outputs=encoder_outputs,
        encoder_final_states=encoder_states,
        scope='decode', reuse=True)
    tf.identity(pred_outputs.sample_id[0], name='predictions_0')

    return tf.estimator.EstimatorSpec(
        mode=mode,
        #######
        # BEAM SEARCH
        # predictions=pred_outputs.predicted_ids[:, :0],
        predictions=pred_outputs.sample_id,
        loss=loss,
        train_op=train_op
    )


def tokenize_and_map(line, vocab):
    return [vocab.get(token, UNK_TOKEN) for token in line.split(' ')]


def make_input_fn(
        batch_size, input_filename, output_filename, vocab,
        input_max_length, output_max_length,
        input_process=tokenize_and_map, output_process=tokenize_and_map):

    def input_fn():
        inp = tf.placeholder(tf.int32, shape=[None, None], name='input')
        output = tf.placeholder(tf.int32, shape=[None, None], name='output')
        tf.identity(inp[0], 'input_0')
        tf.identity(output[0], 'output_0')
        return {
            'input': inp,
            'output': output,
        }, None

    def sampler():
        while True:
            with open(os.path.join(DATA_DIR, input_filename), 'r', encoding='utf-8') as finput:
                with open(os.path.join(DATA_DIR, output_filename), 'r', encoding='utf-8') as foutput:
                    for in_line in finput:
                        out_line = foutput.readline()
                        processed_input = input_process(in_line, vocab)
                        processed_output = output_process(out_line, vocab)
                        if (len(processed_input) > input_min_length) and (len(processed_output) > output_min_length) and (np.equal(processed_input, UNK_TOKEN).astype(int).sum() < input_max_unk):
                            yield {
                                'input': processed_input[:input_max_length - 1] + [END_TOKEN],
                                'output': processed_output[:output_max_length - 1] + [END_TOKEN]
                            }

    sample_me = sampler()

    def feed_fn():
        inputs, outputs = [], []
        input_length, output_length = 0, 0
        for i in range(batch_size):
            rec = next(sample_me)
            inputs.append(rec['input'])
            outputs.append(rec['output'])
            input_length = max(input_length, len(inputs[-1]))
            output_length = max(output_length, len(outputs[-1]))
        # Pad me right with </S> token.
        for i in range(batch_size):
            inputs[i] += [END_TOKEN] * (input_length - len(inputs[i]))
            outputs[i] += [END_TOKEN] * (output_length - len(outputs[i]))
        return {
            'input:0': inputs,
            'output:0': outputs
        }

    return input_fn, feed_fn


def load_vocab():
    vocab = word_to_int
    return vocab


def get_rev_vocab(vocab):
    return {idx: key for key, idx in vocab.items()}


def get_formatter(keys, vocab):
    rev_vocab = get_rev_vocab(vocab)

    def to_str(sequence):
        tokens = [
            rev_vocab.get(x, "<UNK>") for x in sequence]
        return ' '.join(tokens)

    def format(values):
        res = []
        for key in keys:
            res.append("%s = %s" % (key, to_str(values[key])))
        return '\n'.join(res)
    return format


def train_seq2seq(
        input_filename, output_filename,
        model_dir):
    vocab = load_vocab()
    params = PARAMS.copy()

    # write params
    if not os.path.exists(os.path.join(CHECKPOINTS_DIR, MODEL_NAME)):
        os.makedirs(os.path.join(CHECKPOINTS_DIR, MODEL_NAME))
    with open(os.path.join(CHECKPOINTS_DIR, MODEL_NAME, "params.json"), "w", encoding="utf8") as f:
        f.write(json.dumps(PARAMS))

    params['vocab_size'] = len(vocab)
    est = tf.estimator.Estimator(
        model_fn=seq2seq,
        model_dir=model_dir, params=params)

    input_fn, feed_fn = make_input_fn(
        params['batch_size'],
        input_filename,
        output_filename,
        vocab, params['input_max_length'], params['output_max_length'])

    # Make hooks to print examples of inputs/predictions.
    print_inputs = tf.train.LoggingTensorHook(
        ['input_0', 'output_0'], every_n_iter=100,
        formatter=get_formatter(['input_0', 'output_0'], vocab))
    print_predictions = tf.train.LoggingTensorHook(
        ['predictions_0', 'train_pred_0'], every_n_iter=100,
        formatter=get_formatter(['predictions_0', 'train_pred_0'], vocab))

    timeline_hook = timeline.TimelineHook(model_dir, every_n_iter=5000)
    est.train(
        input_fn=input_fn,
        hooks=[tf.train.FeedFnHook(feed_fn), print_inputs, print_predictions],
        # ,timeline_hook],
        steps=100000)


def predict_seq2seq(
        input_filename, output_filename,
        model_dir):
    vocab = load_vocab()
    # params = PARAMS.copy()
    params = {}
    with open(os.path.join(CHECKPOINTS_DIR, MODEL_NAME, "params.json"), "r") as f:
        params = json.load(f)
    params['vocab_size'] = len(vocab)
    est = tf.estimator.Estimator(
        model_fn=seq2seq,
        model_dir=model_dir, params=params)

    input_fn, feed_fn = make_input_fn(
        params['batch_size'],
        input_filename,
        output_filename,
        vocab, params['input_max_length'], params['output_max_length'])

   # Make hooks to print examples of inputs/predictions.
    print_inputs = tf.train.LoggingTensorHook(
        ['input_0', 'output_0'], every_n_iter=1,
        formatter=get_formatter(['input_0', 'output_0'], vocab))
    print_predictions = tf.train.LoggingTensorHook(
        ['predictions_0'], every_n_iter=1,
        formatter=get_formatter(['predictions_0'], vocab))
    for i in est.predict(
            hooks=[tf.train.FeedFnHook(
                feed_fn), print_inputs, print_predictions],
            input_fn=input_fn):
        pass


def main():
    tf.logging._logger.setLevel(logging.INFO)
    handler = logging.FileHandler(os.path.join(
        CHECKPOINTS_DIR, MODEL_NAME + '_predict.log'), 'a', 'utf-8')  # or whatever
    # formatter = logging.Formatter('%(name)s %(message)s')  # or whatever
    # handler.setFormatter(formatter)  # Pass handler as a parameter, not assign
    tf.logging._logger.addHandler(handler)
    train_seq2seq('contents_TTO_train', 'titles_TTO_train',
                  os.path.join(CHECKPOINTS_DIR, MODEL_NAME))

    # predict_seq2seq('contents_TTO_test', 'titles_TTO_test',
    #                 os.path.join(CHECKPOINTS_DIR, MODEL_NAME))


if __name__ == "__main__":
    main()
