from gensim.models import Word2Vec
import os
import numpy as np


def get_dictionary(path, filename):
  model = Word2Vec.load(os.path.join(path, filename))
  vocab = model.wv.vocab
  word_to_int = {}
  int_to_word = [''] * len(vocab)
  for word, vocab_obj in vocab.items():
    index = vocab_obj.index
    int_to_word[index] = word
    word_to_int[word] = index

  embedding_matrix = [[]] * len(vocab)
  for i in range(len(vocab)):
    word = int_to_word[i]
    embedding_matrix[i] = model[word]

  # trim_embedding_matrix = embedding_matrix[
  #     0: vocab_size] if vocab_size else embedding_matrix

  # return word_to_int, int_to_word, trim_embedding_matrix
  return word_to_int, int_to_word, embedding_matrix


def get_helpers(codes=['<S>', '</S>', '<UNK>', '<PAD>']):
  embedding_dim = 128
  word_to_int, int_to_word, embedding_matrix = get_dictionary(
      'data', 'word2vec_gensim_128_50')

  random = np.random.randint(0, len(int_to_word))
  before_adding_codes = embedding_matrix[random]

  int_to_word = codes + int_to_word
  for (word, _) in word_to_int.items():
    word_to_int[word] += len(codes)
  for i in range(len(codes)):
    code = codes[i]
    word_to_int[code] = i
    assert int_to_word[word_to_int[code]] == code
    embedding_matrix = [np.array(np.random.uniform(-1.0, 1.0, embedding_dim),
                                 dtype="float32").tolist()] + embedding_matrix
  embedding_matrix = np.array(embedding_matrix)

  # test
  after_addding_codes = embedding_matrix[random + len(codes)]

  assert(np.equal(before_adding_codes, after_addding_codes)[0])

  return word_to_int, int_to_word, embedding_matrix, embedding_dim
