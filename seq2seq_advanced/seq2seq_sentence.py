import json
import logging
import os

import numpy as np
import tensorflow as tf
from tensorflow.contrib import layers
from .helper_gensim import get_helpers

GO_TOKEN = 0
END_TOKEN = 1
UNK_TOKEN = 2
PAD = 3

DATA_DIR = 'data'
CHECKPOINTS_DIR = 'checkpoints'
MODEL_NAME = 'sentence_titles_scheduled_beam_layers_dropoute_0'
GLOVE_EMBEDDINGS_FILENAME = "zingnews_vectors.txt"
codes = ['<S>', '</S>', '<UNK>', '<PAD>']

word_to_int, int_to_word, embedding_matrix, embedding_dim = get_helpers(
    codes=codes)

input_min_length = 5
output_min_length = 5
input_max_unk = 50

PARAMS = {
    # 'vocab_size': len(vocab),
    'batch_size': 32,
    # 'input_max_length': 500,
    'input_max_sentence': 8,
    'input_max_word_sentence': 40,
    # 'output_max_length': 50,
    'output_max_length': 15,
    'embed_dim': 128,
    'num_units': 512,
    'scheduled_sampling': True,
    # 'beam_search': False,
    'beam_width': 5,
    'num_layers': 2,
    'keep_prob_encoder': 0.6,
    'keep_prob_decoder': 0.6
}


def seq2seq(mode, features, labels, params):
    vocab_size = params['vocab_size']
    embed_dim = params['embed_dim']
    num_units = params['num_units']
    num_layers = params['num_layers']
    keep_prob_encoder = params['keep_prob_encoder'] if mode == tf.estimator.ModeKeys.TRAIN else 1
    keep_prob_decoder = params['keep_prob_decoder'] if mode == tf.estimator.ModeKeys.TRAIN else 1
    # input_max_length = params['input_max_length']

    input_max_sentence = params['input_max_sentence']
    input_max_word_sentence = params['input_max_word_sentence']

    output_max_length = params['output_max_length']

    scheduled_sampling = params['scheduled_sampling']
    inp = features['input']
    output = features['output']
    batch_size = params['batch_size']
    beam_width = params['beam_width']
    # cheat, because GO TOKEN is 0
    start_tokens = tf.zeros([batch_size], dtype=tf.int32)
    train_output = tf.concat([tf.expand_dims(start_tokens, 1), output], 1)
    # input_lengths = tf.reduce_sum(tf.to_int32(tf.not_equal(inp, END_TOKEN)), 1)
    output_lengths = tf.reduce_sum(tf.to_int32(
        tf.not_equal(train_output, END_TOKEN)), 1)

    with tf.variable_scope('embed'):
        #   embeddings = tf.get_variable('embeddings')
        embeddings = tf.get_variable(name="embeddings", shape=embedding_matrix.shape,
                                     initializer=tf.constant_initializer(embedding_matrix), trainable=False)

    # output_embed = layers.embed_sequence(
    # train_output, vocab_size=vocab_size, embed_dim=embed_dim, scope='embed',
    # reuse=True)
    output_embed = tf.nn.embedding_lookup(embeddings, train_output)

    # input_embed = layers.embed_sequence(
    #     inp, vocab_size=vocab_size, embed_dim=embed_dim, scope='embed')

    def sentence_encoder(paragraph):
        input_embed = tf.nn.embedding_lookup(embeddings, paragraph)
        rnn_input = input_embed
        for layer in range(num_layers // 2):
            with tf.variable_scope('sentence_encoder_{}'.format(layer), reuse=False):
                # with tf.variable_scope("sentence_encoder", reuse=False):
                cell_encoder_sentence = tf.contrib.rnn.LSTMCell(
                    num_units=num_units / 2)

                encoder_sentence_outputs, encoder_sentence_state = tf.nn.dynamic_rnn(
                    cell=cell_encoder_sentence,
                    inputs=rnn_input,
                    swap_memory=True,
                    dtype=tf.float32)

                rnn_input = encoder_sentence_outputs
        return encoder_sentence_state[0]

    encoder_sentence_state = tf.map_fn(sentence_encoder, inp, dtype=tf.float32)

    batch_size = tf.shape(inp)[0]
    num_sent = tf.shape(inp)[1]
    num_word_sent = tf.shape(inp)[2]

    # [batch_size, num_sent, num_word_sent, embedding_dim] -> 2 ~ num_word_sent axis
    # encoder_sentence_state = tf.reshape(
    # tf.concat(map_sent, 2), [batch_size, num_sent * num_word_sent,
    # num_units])
    encoder_final_state = None
    paragraph_encoder_inputs = encoder_sentence_state
    for layer in range(num_layers):
        # with tf.variable_scope('sentence_encoder_{}'.format(layer), reuse=False):
        with tf.variable_scope("paragraph_encoder_{}".format(layer), reuse=False):
            cell_encoder_final_fw = tf.contrib.rnn.LSTMCell(
                num_units=num_units)
            cell_encoder_final_fw = tf.contrib.rnn.DropoutWrapper(
                cell_encoder_final_fw, input_keep_prob=keep_prob_encoder)

            cell_encoder_final_bw = tf.contrib.rnn.LSTMCell(
                num_units=num_units)
            cell_encoder_final_bw = tf.contrib.rnn.DropoutWrapper(
                cell_encoder_final_bw, input_keep_prob=keep_prob_encoder)

            encoder_final_outputs, encoder_final_state = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=cell_encoder_final_fw,
                cell_bw=cell_encoder_final_bw,
                inputs=paragraph_encoder_inputs,
                swap_memory=True,
                dtype=tf.float32)
            encoder_final_outputs = tf.concat(encoder_final_outputs, 2)
            paragraph_encoder_inputs = encoder_final_outputs

    encoder_final_state = encoder_final_state[0]  # use fw state only
    encoder_final_outputs = paragraph_encoder_inputs

    def beam_search_decoder(
            scope, beam_width, start_tokens, end_token,
            encoder_final_outputs, encoder_final_state, output_lengths,
            reuse=None):
        with tf.variable_scope(scope, reuse=reuse):
            tiled_encoder_outputs = tf.contrib.seq2seq.tile_batch(
                encoder_final_outputs, multiplier=beam_width)
            tiled_encoder_final_state = tf.contrib.seq2seq.tile_batch(
                encoder_final_state, multiplier=beam_width)
            tiled_output_lengths = tf.contrib.seq2seq.tile_batch(
                output_lengths, multiplier=beam_width)
            attention_mechanism_beam = tf.contrib.seq2seq.LuongAttention(
                num_units=num_units,
                memory=tiled_encoder_outputs,
                memory_sequence_length=tiled_output_lengths,
                scale=True
            )

            cell = tf.contrib.rnn.LSTMCell(
                num_units=num_units)
            cell = tf.contrib.rnn.DropoutWrapper(
                cell, input_keep_prob=keep_prob_decoder)

            # attn_cell
            attn_cell = tf.contrib.seq2seq.AttentionWrapper(
                cell, attention_mechanism_beam, attention_layer_size=num_units)
            # out_cell = tf.contrib.rnn.OutputProjectionWrapper(
            #     attn_cell, vocab_size, reuse=reuse
            # )
            out_cell = attn_cell

        decoder_initial_state_beam = attn_cell.zero_state(
            dtype=tf.float32, batch_size=batch_size * beam_width)
        decoder_initial_state_beam = decoder_initial_state_beam.clone(
            cell_state=tiled_encoder_final_state)

        projection_layer = tf.layers.Dense(
            vocab_size, use_bias=False)

        decoder = tf.contrib.seq2seq.BeamSearchDecoder(
            cell=out_cell,
            embedding=embeddings,
            start_tokens=start_tokens,
            end_token=END_TOKEN,
            initial_state=decoder_initial_state_beam,
            beam_width=beam_width,
            output_layer=projection_layer,
            # length_penalty_weight=0.0
        )

        outputs = tf.contrib.seq2seq.dynamic_decode(
            decoder=decoder, output_time_major=False,
            impute_finished=False, maximum_iterations=output_max_length
        )
        return outputs[0]

    def training_decoder(helper, scope, reuse=None, beam_width=0):
        with tf.variable_scope(scope, reuse=reuse):
            attention_mechanism = tf.contrib.seq2seq.LuongAttention(
                num_units=num_units,
                memory=encoder_final_outputs,
                memory_sequence_length=output_lengths,
                scale=True
            )

            # cell = tf.contrib.rnn.GRUCell(num_units=num_units)
            cell = tf.contrib.rnn.MultiRNNCell(
                [tf.contrib.rnn.LSTMCell(num_units=num_units) for _ in range(num_layers)])
            cell = tf.contrib.rnn.DropoutWrapper(
                cell, input_keep_prob=keep_prob_decoder)

            # attn_cell
            attn_cell = tf.contrib.seq2seq.AttentionWrapper(
                cell, attention_mechanism, attention_layer_size=num_units)
            out_cell = tf.contrib.rnn.OutputProjectionWrapper(
                attn_cell, vocab_size, reuse=reuse
            )

            decoder_initial_state = out_cell.zero_state(
                dtype=tf.float32, batch_size=batch_size)
            decoder = tf.contrib.seq2seq.BasicDecoder(
                cell=out_cell, helper=helper,
                initial_state=decoder_initial_state)
            # initial_state=encoder_final_state)

            outputs = tf.contrib.seq2seq.dynamic_decode(
                decoder=decoder, output_time_major=False,
                swap_memory=True,
                impute_finished=True, maximum_iterations=output_max_length
            )
            return outputs[0]

        # initial_state=encoder_final_state)

        outputs = tf.contrib.seq2seq.dynamic_decode(
            decoder=decoder, output_time_major=False,
            swap_memory=True,
            impute_finished=True, maximum_iterations=output_max_length
        )
        return outputs[0]

    train_helper = tf.contrib.seq2seq.ScheduledEmbeddingTrainingHelper(
        output_embed, output_lengths, embeddings, 0.3) if scheduled_sampling else tf.contrib.seq2seq.TrainingHelper(
        output_embed, output_lengths)
    # train_helper = tf.contrib.seq2seq.ScheduledEmbeddingTrainingHelper(
    #     output_embed, output_lengths, embeddings, 0.3
    # )

    train_outputs = training_decoder(helper=train_helper, scope='decode')
    tf.identity(train_outputs.sample_id[0], name='train_pred_0')

    weights = tf.to_float(tf.not_equal(train_output[:, :-1], 1))
    loss = tf.contrib.seq2seq.sequence_loss(
        train_outputs.rnn_output, output, weights=weights)
    train_op = layers.optimize_loss(
        loss, tf.train.get_global_step(),
        optimizer=params.get('optimizer', 'Adam'),
        learning_rate=params.get('learning_rate', 0.001),
        summaries=['loss', 'learning_rate'])

    if (beam_width):
        ############
        # BEAM SEARCH
        pred_outputs = beam_search_decoder(
            scope='decode',
            beam_width=beam_width,
            start_tokens=start_tokens,
            end_token=END_TOKEN,
            encoder_final_state=encoder_final_state,
            encoder_final_outputs=encoder_final_outputs,
            output_lengths=output_lengths,
            reuse=True
        )

        # predicted_ids[0] best to worst, so get the best first
        # [batch_size, T, beam_width]
        tf.identity(pred_outputs.predicted_ids[0, :, 0], name='predictions_0')
        ############
        return tf.estimator.EstimatorSpec(
            mode=mode,
            #######
            # BEAM SEARCH
            predictions=pred_outputs.predicted_ids[:, :0],
            loss=loss,
            train_op=train_op
        )

    pred_helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
        embeddings, start_tokens=start_tokens, end_token=END_TOKEN)

    pred_outputs = training_decoder(
        helper=pred_helper, scope='decode', reuse=True)
    tf.identity(pred_outputs.sample_id[0], name='predictions_0')

    return tf.estimator.EstimatorSpec(
        mode=mode,
        #######
        # BEAM SEARCH
        # predictions=pred_outputs.predicted_ids[:, :0],
        predictions=pred_outputs.sample_id,
        loss=loss,
        train_op=train_op
    )


def tokenize_and_map(line, vocab):
    return [vocab.get(token, UNK_TOKEN)
            for token in line.rstrip('\n').strip().split(' ')]


def sentence_and_map(line, vocab):
    sentences = line.rstrip("\n").split(' . ')
    result = []
    for sentence in sentences:
        sentence_to_int = [vocab.get(token, UNK_TOKEN)
                           for token in sentence.split(' ')]
        result.append(sentence_to_int)
    return result


def make_input_fn(
        batch_size, input_filename, output_filename, vocab,
        input_max_sentence, input_max_word_sentence, output_max_length,
        input_process=sentence_and_map, output_process=tokenize_and_map,
        concat_output=False):

    def input_fn():
        inp = tf.placeholder(
            tf.int32, shape=[None, None, None], name='input')
        output = tf.placeholder(tf.int32, shape=[None, None], name='output')
        tf.identity(inp[0], 'input_0')
        tf.identity(output[0], 'output_0')
        return {
            'input': inp,
            'output': output,
        }, None

    def sampler():
        while True:
            with open(os.path.join(DATA_DIR, input_filename), 'r', encoding='utf-8') as finput:
                with open(os.path.join(DATA_DIR, output_filename), 'r', encoding='utf-8') as foutput:
                    for in_line in finput:
                        out_line = foutput.readline()
                        processed_input = input_process(in_line, vocab)
                        processed_output = output_process(out_line, vocab)
                        unk_count = np.equal(
                            processed_input, UNK_TOKEN).astype(int).sum()
                        if (len(processed_input) > input_min_length) and (len(processed_output) > output_min_length) and (unk_count < input_max_unk):
                            processed_input = processed_input[:input_max_sentence - 1]
                            processed_input[-1] = processed_input[-1][
                                :input_max_word_sentence - 1] + [END_TOKEN]
                            yield {
                                'input': processed_input,
                                'output': processed_output[:output_max_length - 1] + [END_TOKEN]
                            }

    sample_me = sampler()

    def feed_fn():
        inputs, outputs = [], []
        input_num_sent, input_num_word_sent, output_length = 0, 0, 0
        for i in range(batch_size):
            rec = next(sample_me)
            output = rec['output']
            outputs.append(rec['output'])
            output_length = max(output_length, len(outputs[-1]))

            input = rec['input']
            inputs.append(input)
            input_num_sent = max(input_num_sent, len(inputs[-1]))

            input_num_word_sent = max(input_num_word_sent, np.amax(
                [len(sentence) for sentence in input]))
        # Pad me right with <PAD> token.
        for i in range(batch_size):
            outputs[i] += [PAD] * (output_length - len(outputs[i]))
            for j in range(len(inputs[i])):
                inputs[i][j] += [PAD] * \
                    (input_num_word_sent - len(inputs[i][j]))
                inputs[i] += [[PAD] * input_num_word_sent] * \
                    (input_num_sent - len(inputs[i]))
        # print('INPUTS', inputs[:1])
        return {
            'input:0': np.array(inputs, dtype=np.int32),
            'output:0': outputs
        }

    return input_fn, feed_fn


def load_vocab():
    vocab = word_to_int
    return vocab


def get_rev_vocab(vocab):
    return {idx: key for key, idx in vocab.items()}


def get_formatter(keys, vocab):
    rev_vocab = get_rev_vocab(vocab)

    def to_str(sequence):
        tokens = [
            rev_vocab.get(x, "<UNK>") for x in sequence.flatten() if (x != END_TOKEN) and (x != PAD)]
        return ' '.join(tokens)

    def format(values):
        res = []
        for key in keys:
            res.append("%s = %s" % (key, to_str(values[key])))
        return '\n'.join(res)
    return format


def train_seq2seq(
        input_filename, output_filename,
        model_dir):
    vocab = load_vocab()
    params = PARAMS.copy()
    params['vocab_size'] = len(vocab)

    # write params
    if not os.path.exists(os.path.join(CHECKPOINTS_DIR, MODEL_NAME)):
        os.makedirs(os.path.join(CHECKPOINTS_DIR, MODEL_NAME))
    with open(os.path.join(CHECKPOINTS_DIR, MODEL_NAME, "params.json"), "w", encoding="utf8") as f:
        f.write(json.dumps(PARAMS))

    est = tf.estimator.Estimator(
        model_fn=seq2seq,
        model_dir=model_dir, params=params)

    input_fn, feed_fn = make_input_fn(
        params['batch_size'],
        input_filename,
        output_filename,
        vocab, params['input_max_sentence'],
        params['input_max_word_sentence'],
        params['output_max_length'],
        concat_output=True)
    # Make hooks to print examples of inputs/predictions.
    print_inputs = tf.train.LoggingTensorHook(
        ['input_0', 'output_0'], every_n_iter=100,
        formatter=get_formatter(['input_0', 'output_0'], vocab))
    print_predictions = tf.train.LoggingTensorHook(
        ['predictions_0', 'train_pred_0'], every_n_iter=100,
        formatter=get_formatter(['predictions_0', 'train_pred_0'], vocab))

    # timeline_hook = timeline.TimelineHook(model_dir, every_n_iter=5000)
    est.train(
        input_fn=input_fn,
        hooks=[tf.train.FeedFnHook(feed_fn), print_inputs, print_predictions  # ,
               # timeline_hook
               ],
        steps=200000)


def predict_seq2seq(
        input_filename, output_filename,
        model_dir):
    vocab = load_vocab()
    # params = PARAMS.copy()
    params = {}
    with open(os.path.join(CHECKPOINTS_DIR, MODEL_NAME, "params.json"), "r") as f:
        params = json.load(f)
    params['vocab_size'] = len(vocab)
    est = tf.estimator.Estimator(
        model_fn=seq2seq,
        model_dir=model_dir, params=params)

    input_fn, feed_fn = make_input_fn(
        params['batch_size'],
        input_filename,
        output_filename,
        vocab, params['input_max_sentence'], params['input_max_word_sentence'], params['output_max_length'])

    # Make hooks to print examples of inputs/predictions.
    print_inputs = tf.train.LoggingTensorHook(
        ['input_0', 'output_0'], every_n_iter=1,
        formatter=get_formatter(['input_0', 'output_0'], vocab))
    print_predictions = tf.train.LoggingTensorHook(
        ['predictions_0'], every_n_iter=1,
        formatter=get_formatter(['predictions_0'], vocab))

    for i in est.predict(
            hooks=[
                tf.train.FeedFnHook(
                    feed_fn),
                print_inputs,
                print_predictions
            ],
            input_fn=input_fn):

        pass


def main():
    tf.logging._logger.setLevel(logging.INFO)
    handler = logging.FileHandler(os.path.join(
        CHECKPOINTS_DIR, MODEL_NAME + '.log'), 'w', 'utf-8')  # or whatever
    # formatter = logging.Formatter('%(name)s %(message)s')  # or whatever
    # handler.setFormatter(formatter)  # Pass handler as a parameter, not assign
    tf.logging._logger.addHandler(handler)

    train_seq2seq('contents_train_big_fix_url_quot_textrank', 'titles_train_big_fix_url_quot',
                  os.path.join(CHECKPOINTS_DIR, MODEL_NAME))
    # predict_seq2seq('contents_test', 'titles_test',
    #                 os.path.join(CHECKPOINTS_DIR, MODEL_NAME))


if __name__ == "__main__":
    main()
