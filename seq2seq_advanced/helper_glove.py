import numpy as np

"""
GloVe file format:
[vocab size] [embedding size]
rest is like 
"""
def get_helpers(codes=['<S>', '</S>', '<UNK>', '<PAD>'], glove_embeddings_filepath=''):
    gef = open(glove_embeddings_filepath, mode='r', encoding="utf-8")
    first_line = gef.readline()
    meta = first_line.rstrip("\n").split(' ')
    vocab_size = int(meta[0])
    embedding_dim = int(meta[1])

    # word_to_int is a dict: word - id
    word_to_int = {}
    # int_to_word is a list of str
    int_to_word = [''] * vocab_size
    # embedding_matrix, ultimately, is an np.array
    embedding_matrix = []

    next(gef)
    index = 0
    for line in gef:
        components = line.rstrip("\n").split(' ')
        this_vector = [float(x) for x in components[1:]]
        int_to_word[index] = components[0]
        embedding_matrix.append(this_vector)
        word_to_int[components[0]] = index
        index = index + 1
    gef.close()
    #embedding_matrix = np.array(embedding_matrix)

    random = np.random.randint(0, len(int_to_word))
    before_adding_codes = embedding_matrix[random]

    int_to_word = codes + int_to_word
    for (word, _) in word_to_int.items():
        word_to_int[word] += len(codes)
    for i in range(len(codes)):
        code = codes[i]
        word_to_int[code] = i
        assert int_to_word[word_to_int[code]] == code
        embedding_matrix = [np.array(np.random.uniform(-1.0, 1.0, embedding_dim), dtype="float32").tolist()] + embedding_matrix
    embedding_matrix = np.array(embedding_matrix)

    # test
    after_addding_codes = embedding_matrix[random + len(codes)]

    assert (np.equal(before_adding_codes, after_addding_codes)[0])

    return word_to_int, int_to_word, embedding_matrix, embedding_dim
