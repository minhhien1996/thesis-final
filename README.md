# How to run
- Install all dependencies
```pip install -r requirements.txt ```
- Go to each module and run what you need
# Data
Data are crawled from [Zing News](https://news.zing.vn/) and [Tuoi Tre Online](http://tuoitre.vn).
[Zing News dataset](https://pkamc-my.sharepoint.com/:f:/g/personal/minhhien139_myoffice365_site/EmCoAcNkscBPgywOizDG0SYBSjow8SWIz39KuvvVik_IKQ?e=YfMNoS) 
[Tuoi Tre Online dataset](https://pkamc-my.sharepoint.com/:f:/g/personal/minhhien139_myoffice365_site/EmCoAcNkscBPgywOizDG0SYBSjow8SWIz39KuvvVik_IKQ?e=YfMNoS)
## Data for Word2Vec
Tokens file is a text file, line separated. Each line contains tokens (words), space separated.
## Data for Seq2Seq
Contents and summaries are text files, line separated, same number of lines, each line in the contents file is corresponding to a line in the summaries file.