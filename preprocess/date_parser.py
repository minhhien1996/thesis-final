# -*- coding: utf-8 -*-
# import dateparser
# print(dateparser.parse(u'ngày 7-3-2015', languages=['vi']))
# print(dateparser.parse(u'hôm 9-7', languages=['vi']))
# print(dateparser.parse(u'thập niên 1950', languages=['vi']))
# print(dateparser.parse(u'tháng 8-2015', languages=['vi']))
# print(dateparser.parse(u'2 tháng trước', languages=['vi']))

import re
import num2word_vn


def day_to_string(day):
    pass


delimiter = {
    'day': 'ngày',
    'month': 'tháng',
    'year': 'năm'
}

day_re = r"(?P<day>\d{1,2})(\-(?P<month>\d{1,2}))(\-(?P<year>\d{4}))?"
month_re = r"(?P<month>\d{1,2})(\-(?P<year>\d{4}))"

day_re_full = r"(?P<day>\d{1,2})(\-(?P<month>\d{1,2}))(\-(?P<year>\d{4}))"
day_re_short = r"(?P<day>\d{1,2})(\-(?P<month>\d{1,2}))"


def parse_day(st):
    m = re.match(day_re, st)
    result = ''
    for k, v in m.groupdict().items():
        if v:
            result += delimiter[k] + ' ' + num2word_vn.convert(int(v)) + ' '
    return result.strip()


def parse_month(st):
    m = re.match(month_re, st)
    result = ''
    for k, v in m.groupdict().items():
        if v:
            result += delimiter[k] + ' ' + num2word_vn.convert(int(v)) + ' '
    return result.strip()


def parse_day_from_matched(m):
    d = m.groupdict()
    result = ''
    result += num2word_vn.convert(int(d['day']))
    result += ' ' + delimiter['month'] + ' ' + num2word_vn.convert(int(d['month']))
    result += (' ' + delimiter['year'] + ' ' + num2word_vn.convert(int(d['year']))) if 'year' in d else ''
    return result


def parse_month_from_matched(m):
    d = m.groupdict()
    result = ''
    result += num2word_vn.convert(int(d['month']))
    result += ' ' + delimiter['year'] + ' ' + num2word_vn.convert(int(d['year']))
    return result


def parse_date(st):
    result = st
    result = re.sub(day_re_full, parse_day_from_matched, result)
    result = re.sub(month_re, parse_month_from_matched, result)
    result = re.sub(day_re_short, parse_day_from_matched, result)
    return result


def remove_date(st):
    pass
# print(parse_day("19-10-2014"))
# print(parse_day("7-3-2015"))

# print(parse_month("11-2010"))
