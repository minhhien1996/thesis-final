import os
import random

MIN_SUMMARIES_WORD_COUNT = 20
MIN_TITLES_WORD_COUNT = 6
MIN_CONTENT_WORD_COUNT = 300

PATH = 'output'

IN_SUMMARIES = 'summaries_TTO'
IN_TITLES = 'titles_TTO'
IN_CONTENTS = 'contents_TTO'

TRAIN_SUMMARIES = IN_SUMMARIES + '_train'
TRAIN_TITLES = IN_TITLES + '_train'
TRAIN_CONTENTS = IN_CONTENTS + '_train'

TEST_SUMMARIES = IN_SUMMARIES + '_test'
TEST_TITLES = IN_TITLES + '_test'
TEST_CONTENTS = IN_CONTENTS + '_test'

VALIDATE_SUMMARIES = IN_SUMMARIES + '_validate'
VALIDATE_TITLES = IN_TITLES + '_validate'
VALIDATE_CONTENTS = IN_CONTENTS + '_validate'

TRAIN_RATIO = 0.9
VALIDATE_RATIO = 0.95

TRAIN_SUMMARIES_FILE = open(os.path.join(
    PATH, IN_SUMMARIES + '_train'), mode="w", encoding="utf8")
TRAIN_TITLES_FILE = open(os.path.join(
    PATH, IN_TITLES + '_train'), mode="w", encoding="utf8")
TRAIN_CONTENTS_FILE = open(os.path.join(
    PATH, IN_CONTENTS + '_train'), mode="w", encoding="utf8")

TEST_SUMMARIES_FILE = open(os.path.join(
    PATH, IN_SUMMARIES + '_test'), mode="w", encoding="utf8")
TEST_TITLES_FILE = open(os.path.join(
    PATH, IN_TITLES + '_test'), mode="w", encoding="utf8")
TEST_CONTENTS_FILE = open(os.path.join(
    PATH, IN_CONTENTS + '_test'), mode="w", encoding="utf8")

VALIDATE_SUMMARIES_FILE = open(os.path.join(
    PATH, IN_SUMMARIES + '_validate'), mode="w", encoding="utf8")
VALIDATE_TITLES_FILE = open(os.path.join(
    PATH, IN_TITLES + '_validate'), mode="w", encoding="utf8")
VALIDATE_CONTENTS_FILE = open(os.path.join(
    PATH, IN_CONTENTS + '_validate'), mode="w", encoding="utf8")


with open(os.path.join(PATH, IN_SUMMARIES), mode="r", encoding="utf8") as in_sum, open(os.path.join(PATH, IN_TITLES), mode="r", encoding="utf8") as in_tit, open(os.path.join(PATH, IN_CONTENTS), mode="r", encoding="utf8") as in_cont:
  for summary in in_sum:
    title = in_tit.readline()
    content = in_cont.readline()
    if len(summary.split()) >= MIN_SUMMARIES_WORD_COUNT and len(title.split()) >= MIN_TITLES_WORD_COUNT and len(content.split()) >= MIN_CONTENT_WORD_COUNT:
      r = random.random()
      if (r <= TRAIN_RATIO):
        TRAIN_CONTENTS_FILE.write(content)
        TRAIN_SUMMARIES_FILE.write(summary)
        TRAIN_TITLES_FILE.write(title)
      elif (r > TRAIN_RATIO) and (r < VALIDATE_RATIO):
        VALIDATE_CONTENTS_FILE.write(content)
        VALIDATE_SUMMARIES_FILE.write(summary)
        VALIDATE_TITLES_FILE.write(title)
      else:
        TEST_CONTENTS_FILE.write(content)
        TEST_SUMMARIES_FILE.write(summary)
        TEST_TITLES_FILE.write(title)

TRAIN_CONTENTS_FILE.close()
TRAIN_SUMMARIES_FILE.close()
TRAIN_TITLES_FILE.close()
VALIDATE_CONTENTS_FILE.close()
VALIDATE_SUMMARIES_FILE.close()
VALIDATE_TITLES_FILE.close()
TEST_CONTENTS_FILE.close()
TEST_SUMMARIES_FILE.close()
TEST_TITLES_FILE.close()
