import os

PATH = 'output'
SUM = 'summaries_TTO'
CON = 'contents_TTO'
TIT = 'titles_TTO'
TOK = 'tokens_TTO'

CPU_NUM = os.cpu_count()
SF = open(os.path.join(PATH, SUM), mode='w', encoding='utf-8')
CF = open(os.path.join(PATH, CON), mode='w', encoding='utf-8')
TF = open(os.path.join(PATH, TIT), mode='w', encoding='utf-8')
for i in range(CPU_NUM):
  SUM_ = SUM + '-' + str(i)
  CON_ = CON + '-' + str(i)
  TIT_ = TIT + '-' + str(i)
  TOK_ = TOK + '-' + str(i)
  with open(os.path.join(PATH, TIT_), mode='r', encoding='utf-8') as f1, open(os.path.join(PATH, SUM_), mode='r', encoding='utf-8') as f2, open(os.path.join(PATH, CON_), mode='r', encoding='utf-8') as f3, open(os.path.join(PATH, TOK_), mode='w', encoding='utf-8') as f4:
    for title, summary, content in zip(f1, f2, f3):
      SF.write(summary)
      TF.write(title)
      CF.write(content)
      title = title.strip()
      summary = summary.strip()
      content = content.strip()
      tokens = "{0} {1} {2}".format(title, summary, content)
      f4.write(tokens + '\n')
      
SF.close()
TF.close()
CF.close()