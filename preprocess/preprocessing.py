# -*- coding: utf-8 -*-
""" This module help preprocess vietnamese text """
import re
import unicodedata
from underthesea import word_sent
from date_parser import parse_date
from num2word_vn import parse_number

def preprocess(text='',
               dot_only=True,
               should_lower_first_word=True,
               should_tokenize=True,
               no_inner_punctual=True,
               date_to_word=False,
               number_to_word=False,
               no_accent=False,
               to_lower=True):
  """ preprocess text and return the result """
  # ensure the final char is dot, because some text miss this
  result = text + '.'
  if dot_only:
    result = remove_but_keep_dot(result)
  if should_lower_first_word:
    result = lower_first_word(result)
  if should_tokenize:
    result = word_sent(result, format="text")
  if no_inner_punctual:
    result = remove_inner_punctuation(result)
  if date_to_word:
    result = parse_date(result)
  if number_to_word:
    result = parse_number(result)
  if no_accent:
    result = no_accent_vietnamese(result)
  if to_lower:
    result = result.lower()
  return result

def remove_inner_punctuation(text):
  """ Remove inner punctuation of a sentence """
  result = text
  # elipsis \u2026
  # left right single quotation mark \u2018 \u2019
  # zero width space \u200b
  # left right double quotation mark \u201c \u201d
  result = re.sub(
      r"[ ()\-:/,&+><=;\u201c\u201d\"'\u2018\u2019\u200b\u2013\u2026]+", ' ', result)
  return result

def remove_but_keep_dot(text):
  """ Change other punctuations such as question mark, exclamation mark to dot """
  result = text
  result = re.sub(r"[?!]", '.', result)
  # multiple dots to one dot
  result = re.sub(r"(\.){2,}", '.', result)
  return result

def lower_first_word (paragraph):
  sentences = paragraph.split(". ")
  sentences2 = []
  for sentence in sentences:
      if len(sentence) > 1:
          sentences2.append(sentence[0].lower() + sentence[1:])
      elif len(sentence) == 1:
          sentences2.append(sentence[0].lower())
  paragraph2 = '. '.join(sentences2)
  return paragraph2

def no_accent_vietnamese(s):
    s = re.sub(u'Đ', 'D', s)
    s = re.sub(u'đ', 'd', s)
    return unicodedata.normalize('NFKD', s).encode('ASCII', 'ignore').decode()
