from pymongo import MongoClient
from preprocessing import preprocess

CONNECTION_STRING = "mongodb://scrapy_reader:123456@139.59.118.55:27017/scrapy?authMechanism=SCRAM-SHA-1&authSource=scrapy"
# CONNECTION_STRING = "mongodb://scrapy_reader:123456@localhost:27017/scrapy?authMechanism=SCRAM-SHA-1&authSource=scrapy"
client = MongoClient(CONNECTION_STRING)

db = client['scrapy']
tuoitreCollection = db['tuoitre']

for doc in tuoitreCollection.find():
    output_file = open('../text_tokenization/input/' + str(doc[u'_id']), 'w')
    content = preprocess(str(doc[u"raw_content"]), no_accent=False, date_to_word=False, number_to_word=False,
                         no_punctual_but_dot=True, to_lower=False)
    output_file.write(content)
    output_file.close()
