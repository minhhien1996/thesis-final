"""
This module create files containing all text for word2vec training.
Output:
CONTENTS_FILE: All articles' contents, line by line
SUMMARIES_FILE: All summaries, line by line
TOKENS_FILE: All tokens, line by line
"""
from pymongo import MongoClient, ASCENDING
from preprocessing import preprocess
import re
import os
from multiprocessing import Pool

LEN_CONTENT = 400
LEN_SUMMARY = 30

# CONNECTION_STRING = "mongodb://scrapy_reader:123456@139.59.118.55:27017/scrapy?authMechanism=SCRAM-SHA-1&authSource=scrapy"
CONNECTION_STRING = "mongodb://localhost:27017/scrapy"
CLIENT = MongoClient(CONNECTION_STRING)
DB = CLIENT['scrapy']
# COLLECTION = DB['zingnews']
COLLECTION = DB['tuoitre']
# CONTENTS_FILE = open("output/contents", "w", encoding="utf-8")
# SUMMARIES_FILE = open("output/summaries", "w", encoding="utf-8")
# TOKENS_FILE = open("output/tokens", "w", encoding="utf-8")

CPU_NUM = os.cpu_count()
TOTAL_DOCS = COLLECTION.count()
BATCH_SIZE = TOTAL_DOCS // CPU_NUM


def process_batch(cpu_id):
  CONTENTS_FILE = open(os.path.join("output", "contents_TTO") + '-' +
                       str(cpu_id), "w", encoding="utf-8")
  SUMMARIES_FILE = open(os.path.join("output", "summaries_TTO") + '-' +
                        str(cpu_id), "w", encoding="utf-8")
  TITLES_FILE = open(os.path.join("output", "titles_TTO") + '-' +
                     str(cpu_id), "w", encoding="utf-8")
  docs = COLLECTION.find(no_cursor_timeout=True).sort(
      '_id', ASCENDING).skip(cpu_id * BATCH_SIZE).limit(BATCH_SIZE)
  for doc in docs:
    print("Processing " + str(doc[u'_id']))
    org_content = preprocess(str(doc[u"raw_content"])).strip()
    org_summary = preprocess(str(doc[u"summarization"])).strip()

    org_title = str(doc[u"title"]).strip()
    if not org_title.endswith('.'): org_title += '.'
    org_title = preprocess(org_title).strip()

    content = org_content + "\n"
    summary = org_summary + "\n"
    title = org_title + "\n"
    if (len(content) > (LEN_CONTENT)) and (len(summary) > (LEN_SUMMARY)):
      CONTENTS_FILE.write(content)
      SUMMARIES_FILE.write(summary)
      TITLES_FILE.write(title)
  CONTENTS_FILE.close()
  SUMMARIES_FILE.close()
  TITLES_FILE.close()
  docs.close()

if __name__ == '__main__':
  pool = Pool()
  pool.map(process_batch, range(CPU_NUM))

# docs = COLLECTION.find(no_cursor_timeout=True)  # .limit(10)

# for doc in docs:
#   print("Processing " + str(doc[u'_id']))
#   org_content = preprocess(str(doc[u"content"])).strip()
#   org_summary = preprocess(str(doc[u"summary"])).strip()
#   content = org_content + "\n"
#   summary = org_summary + "\n"
#   tokens = re.sub(r"\.+", ".", (org_summary + ". " + org_content).strip())
#   if (len(content) > (LEN_CONTENT)) and (len(summary) > (LEN_SUMMARY)):
#     CONTENTS_FILE.write(content)
#     SUMMARIES_FILE.write(summary)
#     TOKENS_FILE.write(tokens)
# CONTENTS_FILE.close()
# SUMMARIES_FILE.close()
# TOKENS_FILE.close()
# docs.close()
