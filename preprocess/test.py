# -*- coding: utf-8 -*-
import re
import unicodedata

s = "Google, chứ không phải Facebook, mới là hãng lưu nhiều thông tin của người dùng nhất. Vậy tại sao chưa ai đề cập tới chuyện này?"
#s = re.sub(u'Đ', 'D', s)
#s = re.sub(u'đ', 'd', s)
result = unicodedata.normalize('NFKD', s).encode('ASCII', 'ignore').decode()
print(result.lower())