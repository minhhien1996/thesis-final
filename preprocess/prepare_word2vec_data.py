"""This module create a file contains all text for word2vec training"""
from pymongo import MongoClient
from preprocessing import preprocess
LEN_CONTENT = 400
LEN_SUMMARY = 40
# CONNECTION_STRING = "mongodb://scrapy_reader:123456@139.59.118.55:27017/scrapy?authMechanism=SCRAM-SHA-1&authSource=scrapy"
CONNECTION_STRING = "mongodb://localhost:27017/scrapy"
CLIENT = MongoClient(CONNECTION_STRING)
DB = CLIENT['scrapy']
COLLECTION = DB['zingnews']
OUTPUT_FILE = open("output/tokens", "w", encoding="utf-8")
# countdown = 10
for doc in COLLECTION.find():
  # countdown = countdown - 1
  # if countdown == 0:
  #     break
  print("Processing " + str(doc[u'title']))
  org_content = str(doc[u"summary"]) + ". " + str(doc[u"content"])
  content = preprocess(org_content).strip() + " "
  OUTPUT_FILE.write(content)
  # if (len(content) > (LEN_SUMMARY + LEN_CONTENT)):
  #   OUTPUT_FILE.write(content)
OUTPUT_FILE.close()
